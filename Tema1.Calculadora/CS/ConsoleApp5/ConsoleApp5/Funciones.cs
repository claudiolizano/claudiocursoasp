﻿using System;

namespace ConsoleApp5
{
    class Funciones
    {
        static void Main(string[] args)
        {
            var nombre = "Gandalf";
            SayHello(nombre);
            SayHello();


            var suma = Sum(5, 76);
            Console.WriteLine($"la suma es {suma}");
        }
        public static void SayHello(string name)
        {
            
            Console.WriteLine("Hola " + name);
            Console.WriteLine($"Hola {name}");
            Console.WriteLine(string.Format("Hola {0}", name));
        }
          //Otra tipo de funciones.

        public static int Sum(int num1, int num2)
        {
            var suma = num1 + num2;
            return suma;
        }
        
        //podemos encontar funciones vacias que no devolverían nada.
        public static void SayHello()
        {
            Console.WriteLine("Hola")
        }

    }

}
