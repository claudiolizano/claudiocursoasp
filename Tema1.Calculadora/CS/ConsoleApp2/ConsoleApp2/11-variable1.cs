﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {

            var num1 = 5;
            var num2 = 8;

            var suma = num1 + num2;

            Console.WriteLine("La suma es:" + suma);

            num1 = 2;
            suma = num1 + num2;

            Console.WriteLine("La suma es:" + suma);

        }
    }
}
