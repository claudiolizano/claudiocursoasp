﻿using System;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escriba un numero");
            var inputText = Console.ReadLine();

            var num1 = int.Parse(inputText);
        }

         static void Declaracion1()
        { 
            // ejems
            int num1 = 5;
            double num2 = 5.0;

            // otro tipo
            bool condition1 = true;
            bool condition2 = false;

            // otro forma

            string text = "popote";
            char c1 = 'p';

            //other

            DateTime fecha = new DateTime();
            DateTime fechaDeHoy = new DateTime.Now;


            Console.WriteLine("Hello World!");
        }

            //OTRA FOIRMA DE DECLARACION

        static void Declaration2()
        {
            var num1 = 5;
            var num2 = 5.0;

            // otro tipo
            var condition1 = true;
            var condition2 = false;

            // otro forma

            var text = "popote";
            var c1 = 'p';

            //other

            var fecha = new DateTime();
            var fechaDeHoy = new DateTime.Now;


        }



    }
}
