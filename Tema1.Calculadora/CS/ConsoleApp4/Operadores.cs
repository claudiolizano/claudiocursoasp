﻿using System;

namespace ConsoleApp4
{
    class Operadores
    {
        static void Main(string[] args)
        {

            var num1 = 5;
            var num2 = 6;

            var suma = num1 + num2;
            var resta = num1 - num2;
            var multiplicacion = num1 * num2;
            var div = num1 / num2;


            var operacion1 = (5 + 8) / 5 * 8;

            var message = "Hola";
            var name = "Gandalf";

            var text = message + name;

            //var text2 = message - name; --->ERROR 

            // aqui podemos add
            num1 = num1 + 3;  
            num1 += 3;
            num1 *= 2;

            var num3 = num1++; --->// El num suma +1

        }
    }
}
