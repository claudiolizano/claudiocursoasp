﻿using System;

namespace VariableBucle
{
    class Program
    {
        static void Main(string[] args)
        {
            var nom1 = "Claudio ";
            var apell2 = " Lizano";
            var apell3 = " Castañeda";
            var suma = nom1 + apell2 + apell3;

            var respuesta = "My nombre es " + suma;

            Console.WriteLine(respuesta);
        }
    }
}
