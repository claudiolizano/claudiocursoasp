﻿using System;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            var notaDePepe = 10.00;
            var notasDeCris = 9.99;

            var n = 20;

            //coleccion de notas de todos los alumnos Ejem 20..
            // Se puede poner: double[] notas = new double[n]
            var notas = new double[n];

            notas[0] = 10.00;
            notas[1] = 9.99;
              //se puede poner [ n -1] = 19.
            notas[n] = 5;

            Console.WriteLine("Hello World!");
        }
    }
}
