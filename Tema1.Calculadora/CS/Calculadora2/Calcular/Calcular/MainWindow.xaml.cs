﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calcular
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        double Num1, Num2, Resultado;

        private void BtSuma_Click(object sender, RoutedEventArgs e)
        {
            var Num1 = Convert.ToDouble(LbNum1.Text);
            var Num2 = Convert.ToDouble(LbNum2.Text);

            var Resultado = Num1 + Num2;


            LbOutput.Text = String.Format("{0:F2}", Resultado);

        }


        private void BtResta_Click(object sender, RoutedEventArgs e)
        {
            var Num1 = Convert.ToDouble(LbNum1.Text);
            var Num2 = Convert.ToDouble(LbNum2.Text);

            var Resultado = Num1 - Num2;


            LbOutput.Text = String.Format("{0:F2}", Resultado);
        }


        private void BtMultiply_Click(object sender, RoutedEventArgs e)
        {
            var Num1 = Convert.ToDouble(LbNum1.Text);
            var Num2 = Convert.ToDouble(LbNum2.Text);

            var Resultado = Num1 * Num2;


            LbOutput.Text = String.Format("{0:F2}", Resultado);
        }


        private void BtDivide_Click(object sender, RoutedEventArgs e)
        {
            var Num1 = Convert.ToDouble(LbNum1.Text);
            var Num2 = Convert.ToDouble(LbNum2.Text);

            var Resultado = Num1 / Num2;


            LbOutput.Text = String.Format("{0:F2}", Resultado);
        }
    }
}
