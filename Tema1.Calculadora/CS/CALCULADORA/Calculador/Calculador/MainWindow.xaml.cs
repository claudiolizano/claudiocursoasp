﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculador
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
          
        }
       
        #Buttons Numbers

        private void BtPress7_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "7";
        }

        private void BtPress8_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "8";
        }

        private void BtPress9_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "9";
        }

        private void BtPress4_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "4";
        }

        private void BtPress5_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "5";
        }

        private void BtPress6_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "6";
        }

        private void BtPress1_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "1";
        }

        private void BtPress2_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "2";
        }

        private void BtPress3_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "3";
        }
       
        #endregion



        private void BtPressAdd_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "Add";
        }

        private void BtPress0_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "0";
        }

        private void BtPressCalcular_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "Calc";
        }

        private void BtPressSubstract_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "Substract";
        }

        private void BtPressmultiply_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "Multiply";
        }

        private void BtPressDivide_Click(object sender, RoutedEventArgs e)
        {
            TbResult.Text += "Divide";
        }

        
    }
}
