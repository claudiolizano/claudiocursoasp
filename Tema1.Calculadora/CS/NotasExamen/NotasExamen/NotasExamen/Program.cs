﻿using System;

namespace NotasExamen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenidos al programa de gestion de alumnos");
            Console.WriteLine("Introduzca las notas de los alumnos");

            // 1 cambiar el tamaño del array a 10 y meter 10 notas en vez 5
            var notasDeAlumnos = new double[10];

            notasDeAlumnos[0] = 4.6;
            notasDeAlumnos[1] = 2.3;
            notasDeAlumnos[2] = 6;
            notasDeAlumnos[3] = 3.9;
            notasDeAlumnos[4] = 8.5;
            notasDeAlumnos[5] = 4.0;
            notasDeAlumnos[6] = 7.4;
            notasDeAlumnos[7] = 9;
            notasDeAlumnos[8] = 5.9;
            notasDeAlumnos[9] = 4.5;

            // 2 ajustar la media
            var suma = notasDeAlumnos[0] +
                        notasDeAlumnos[1] +
                        notasDeAlumnos[2] +
                        notasDeAlumnos[3] +
                        notasDeAlumnos[4] +
                        notasDeAlumnos[5] +
                        notasDeAlumnos[6] +
                        notasDeAlumnos[7] +
                        notasDeAlumnos[8] +
                        notasDeAlumnos[9];

            var media = suma / notasDeAlumnos.Length;
            Console.WriteLine($"la media es {media}");


            // 3 extraer la nota más alta del array y enseñarla en pantalla

           
             
             var max = notasDeAlumnos[0];

            if (notasDeAlumnos[1] > max)
                max = notasDeAlumnos[1];

            if (notasDeAlumnos[2] > max)
                max = notasDeAlumnos[2];

            if (notasDeAlumnos[3] > max)
                max = notasDeAlumnos[3];

            if (notasDeAlumnos[4] > max)
                max = notasDeAlumnos[4];

            if (notasDeAlumnos[5] > max)
                max = notasDeAlumnos[5];

            if (notasDeAlumnos[6] > max)
                max = notasDeAlumnos[6];

            if (notasDeAlumnos[7] > max)
                max = notasDeAlumnos[7];

            if (notasDeAlumnos[8] > max)
                max = notasDeAlumnos[8];

            if (notasDeAlumnos[9] > max)
                max = notasDeAlumnos[9]; 
            


        Console.WriteLine($"El máximo valor es {max}");

        
            // 4 extraer la nota más baja del array y enseñarña en pantalla

            var min = notasDeAlumnos[0];

            if (notasDeAlumnos[1] < min)
                min = notasDeAlumnos[1];

            if (notasDeAlumnos[2] < min)
                min = notasDeAlumnos[2];

            if (notasDeAlumnos[3] < min)
                min = notasDeAlumnos[3];

            if (notasDeAlumnos[4] < min)
                min = notasDeAlumnos[4];

            if (notasDeAlumnos[5] < min)
                min = notasDeAlumnos[5];

            if (notasDeAlumnos[6] < min)
                min = notasDeAlumnos[6];

            if (notasDeAlumnos[7] < min)
                min = notasDeAlumnos[7];

            if (notasDeAlumnos[8] < min)
                min = notasDeAlumnos[8];

            if (notasDeAlumnos[9] < min)
                min = notasDeAlumnos[9];


            Console.WriteLine($"El mínimo valor es {min}");


        }
    }
}
