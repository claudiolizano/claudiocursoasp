﻿using System;

namespace OBJETOS_CLASES
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hola PEPE!");
              // Este es una programacion estructurada

            //Forma 1.
            var pepe = new Persona();
            pepe.Name = "Pepe";
            pepe.Year = 1978;
            pepe.Email = "a@a.com";

            //Forma 2.
            //Este accede al constructor de la forma public persona con this.
            var lolo = new Persona("lolo",2019, "b@b");


            //forma 3 es igual exactat a la forma 1.

            var maria = new Persona
            {
                Name = "Maria",
                Year = 1978,
                Email = "m@m.com"
            };

            ShowPersonaInformation(pepe);
            //Este viene de public void ShowInfo. en este caso ya no se necesita la parte de arriba
            // que es ShowPersonaInformation(pepe) que antes era solo de pepe..
            pepe.ShowInfo();

            //Forma 4.
            //Aqui podemos  agregar otra forma de hacer.

            ShowPersonaInformation(pepe);
            ShowPersonaInformation(lolo);
            ShowPersonaInformation(maria);
        //Que es exactamente igual a:
            Console.writeLine();
            pepe.ShowInfo();
            lolo.ShowInfo();
            maria.ShowInfo();

         }  

        static void ShowPersonaInformation(Persona persona)
        {

        Console.WriteLine("funcion estatica: la persona con nombre " + persona.Name + " que nació en el año del señor " + persona.Year.ToString() + " y que tiene  de email" + persona.Email);
                
        }
    }

    class Persona
    {
        public string Name { get; set; }
        public string Year { get; set; }
        public string Email { get; set; }


        //A este constructor vacio pueden entrar las dos: forma 1 y forma3.
        public Persona()

        {

        }
         
        //A este constructor accede la forma 2.
        public Persona(string name, int year, string email)
        {
            this.Name = name;
            this.Year = year;
            this.Email = email;

            ShowInfo();
        }

          //en este ya no necesitamos persona sino aplicamos this.
        public void ShowInfo()
        {
           Console.WriteLine("Metodo de clase: la persona con nombre " + this.Name + " que nació en el año del señor " + this.Year.ToString() + " y que tiene  de email" + this.Email);
        }
    }
}

