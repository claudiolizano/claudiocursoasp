﻿using System;

namespace ConsoleApp10
{
    class Inicio
    {
        static void Main(string[] args)
        {
            var notaDePepe = 10.00;
            var notaDeCris = 9.99;

            var n = 20;

            var notas = new double[n];

            notas[0] = 10.0;
            notas[1] = 9.99;

            notas[n - 1] = 5;

            //other forms de declaration: EJEMPLO DE DICCIONARIO
            //Se explica en el video de diccionario.
            private static void EjemploDiccionario()
            {

                var diccionario = new Diccionary<string, int>();
                diccionario.Add("A1", 100);

                if (!diccionario.ContainsKey("A2"))
                {
                    diccionario.Add("A2", 300);
                }

                else
                {
                    //esto daria error!!!!!.
                    diccionario["A5"] = 500;
                }

                foreach (var par in diccionario)
                {
                    Console.WriteLine($"La la clave {par.key} tiene como valor {par.value}");
                }
                foreach (var valor in diccionario.values)
                {
                    Console.WriteLine($"{valor}");
                }
    }
}
